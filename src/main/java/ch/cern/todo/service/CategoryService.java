package ch.cern.todo.service;

import ch.cern.todo.entity.Category;
import ch.cern.todo.repository.CategoryRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    public Category save(Category category) {
        return categoryRepository.save(category);
    }

    public List<Category> findAll() {
        return new ArrayList<>(categoryRepository.findAll());
    }

    public Optional<Category> findById(Long id) {
        return categoryRepository.findById(id);
    }
}
