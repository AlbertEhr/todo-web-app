package ch.cern.todo.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "TASK_CATEGORIES", indexes = {
        @Index(name = "name", columnList = "CATEGORY_NAME", unique = true)})
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Category {

    @Id
    @GeneratedValue
    @Column(name = "CATEGORY_ID")
    private Long id;

    @Column(name = "CATEGORY_NAME", length = 100, unique = true, nullable = false)
    private String name;

    @Column(name = "CATEGORY_DESCRIPTION", length = 500)
    private String description;
}
