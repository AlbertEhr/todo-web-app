package ch.cern.todo.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryCreationDTO {

    private String name;
    private String description;
}
