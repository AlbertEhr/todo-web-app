package ch.cern.todo.dto;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Getter
@Setter
public class TaskCreationDTO {

    private String name;
    private String description;
    private Timestamp deadline;
    private Long categoryID;
}
