package ch.cern.todo.dto;

import ch.cern.todo.entity.Category;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Getter
@Setter
public class TaskDTO {

    private Long id;
    private String name;
    private String description;
    private Timestamp deadline;
    private Category category;
}
