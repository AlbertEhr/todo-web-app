package ch.cern.todo.util;

import ch.cern.todo.dto.CategoryCreationDTO;
import ch.cern.todo.dto.CategoryDTO;
import ch.cern.todo.dto.TaskCreationDTO;
import ch.cern.todo.dto.TaskDTO;
import ch.cern.todo.entity.Category;
import ch.cern.todo.entity.Task;
import org.springframework.stereotype.Component;



@Component
public class Mapper {
    public CategoryDTO toDto(Category category) {

        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setId(category.getId());
        categoryDTO.setName(category.getName());
        categoryDTO.setDescription(category.getDescription());

        return categoryDTO;
    }

    public TaskDTO toDto(Task task) {

        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setDeadline(task.getDeadline());
        taskDTO.setCategory(task.getCategory());

        return taskDTO;
    }

    public Category toCategory(CategoryCreationDTO categoryDTO) {

        Category category = new Category();
        category.setName(categoryDTO.getName());
        category.setDescription(categoryDTO.getDescription());

        return category;
    }

    public Task toTask(TaskCreationDTO taskCreationDTO, Category category) {

        Task task = new Task();
        task.setName(taskCreationDTO.getName());
        task.setDescription(taskCreationDTO.getDescription());
        task.setDeadline(taskCreationDTO.getDeadline());
        task.setCategory(category);

        return task;
    }
}
