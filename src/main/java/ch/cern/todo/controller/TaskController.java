package ch.cern.todo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import ch.cern.todo.dto.TaskCreationDTO;
import ch.cern.todo.dto.TaskDTO;
import ch.cern.todo.entity.Task;
import ch.cern.todo.entity.Category;
import ch.cern.todo.service.CategoryService;
import ch.cern.todo.service.TaskService;
import ch.cern.todo.util.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api")
public class TaskController {

    @Autowired
    TaskService taskService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    Mapper mapper;

    private static final Long GENERAL_CATEGORY_ID = 1L;

    @GetMapping(path = "/tasks", produces = "application/json")
    public ResponseEntity<List<TaskDTO>> getAllTasks() {
        try {
            List<Task> tasks = new ArrayList<>(taskService.findAll());
            if (tasks.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            List<TaskDTO> taskDTOs = tasks.stream().map(mapper::toDto).collect(Collectors.toList());
            return new ResponseEntity<>(taskDTOs, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/tasks", consumes = "application/json", produces = "application/json")
    public ResponseEntity<TaskDTO> createTask(@RequestBody TaskCreationDTO taskCreationDTO) {
        Optional<Category> category;
        if (taskCreationDTO.getCategoryID() == null) {
            category = categoryService.findById(GENERAL_CATEGORY_ID);
        } else {
            category = categoryService.findById(taskCreationDTO.getCategoryID());
        }
        Task task = mapper.toTask(taskCreationDTO, category.get());

        try {
            Task newTask = taskService.save(task);

            return new ResponseEntity<>(mapper.toDto(newTask), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}