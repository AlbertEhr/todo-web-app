package ch.cern.todo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import ch.cern.todo.dto.CategoryCreationDTO;
import ch.cern.todo.dto.CategoryDTO;
import ch.cern.todo.entity.Category;
import ch.cern.todo.service.CategoryService;
import ch.cern.todo.util.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api")
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @Autowired
    Mapper mapper;

    @GetMapping(path = "/categories", produces = "application/json")
    public ResponseEntity<List<CategoryDTO>> getAllCategories() {
        try {
            List<Category> categories = new ArrayList<>(categoryService.findAll());
            if (categories.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            List<CategoryDTO> categoriesDTO = categories.stream().map(mapper::toDto).collect(Collectors.toList());
            return new ResponseEntity<>(categoriesDTO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/categories", consumes = "application/json", produces = "application/json")
    public ResponseEntity<CategoryDTO> createCategory(@RequestBody CategoryCreationDTO categoryCreationDTO) {

        Category category = mapper.toCategory(categoryCreationDTO);

        try {
            Category newCategory = categoryService.save(category);

            return new ResponseEntity<>(mapper.toDto(newCategory), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}